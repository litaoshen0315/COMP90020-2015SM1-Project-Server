# Rabbit-Hunting-Game-Server - Not used #

(This repository will not be maintained any more since all the related codes are moved to COMP90020-2015SM1-Project)

This is a part of student project of COMP90020 Distributed Algorithms in the University of Melbourne at 2015 Semester 1

The centralised server for the Rabbit-Hunting-Game. Using the basic central server algorithm to handle the mutual exclustion problem.

## Authors ##

* Xun Guo

* Litao Shen
