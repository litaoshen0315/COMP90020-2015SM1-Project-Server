package centralserver;

import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Bridge {
	public String on_bridge;
	public ArrayList<String> waiting;
	public int id;
	
	private ReadWriteLock waiting_lock;
	
	public Bridge(int id) {
		this.on_bridge = "0";
		this.waiting = new ArrayList<String>();
		this.id = id;
		
		this.waiting_lock = new ReentrantReadWriteLock();
	}
	
	public synchronized boolean enter(Player player) throws InterruptedException {
		String player_id = Integer.toString(player.id);
		if (!this.waiting.contains(player_id)) {
			addToWaiting(player_id);
			player.setWaiting(this);
		}
		if (!on_bridge.equals("0") && !on_bridge.equals(player_id)) {
			printWaitingList();
			return false;
		}
		on_bridge = player_id;
		player.setOccupying(this);
		player.setWaiting(null);
		removeFromWaiting(player_id);
		notify();
		return true;
	}
	
	public synchronized void leave(Player player) throws InterruptedException {
		while (on_bridge.equals("0")) {
			wait();
		}
		on_bridge = "0";
		player.setOccupying(null);
		notify();
	}
	
	public synchronized void discardWaiting(Player player) {
		String player_id = Integer.toString(player.id);
		if (this.waiting.contains(player_id)) {
            removeFromWaiting(player_id);
            player.setWaiting(null);
        }
	}
	
	public void addToWaiting(String player) {
		try {
			this.waiting_lock.writeLock().lock();;
			this.waiting.add(player);
		} finally {
			this.waiting_lock.writeLock().unlock();
		}	
	}
	
	public void removeFromWaiting(String player) {
		try {
			this.waiting_lock.writeLock().lock();
			this.waiting.remove(player);
		} finally {
			this.waiting_lock.writeLock().unlock();
		}
	}
	
	public void printWaitingList() {
		String waiting_print = "Waiting list of bridge[" + this.id + "]: ";
		if (!this.waiting.isEmpty()) {
			for (String p: this.waiting) {
				waiting_print += p + " ";
			}
			System.out.println(waiting_print);
		}
	}
}
