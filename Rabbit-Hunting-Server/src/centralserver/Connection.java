package centralserver;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Connection extends Thread {

	SystemStatus system_status;
	Socket socket;
	BufferedReader in;
	PrintWriter out;
	
	Player player;
	int winner_id;
	int bridge_id;
	int target_id;
	
	public Connection(SystemStatus system_status, Socket socket, Player player) {
		this.system_status = system_status;
		this.socket = socket;
		this.player = player;
		
		try {
			this.in = new BufferedReader(new InputStreamReader(
					this.socket.getInputStream()));
			this.out = new PrintWriter(this.socket.getOutputStream(), true);
			
			out.println(Protocol.returnUserId(player.id));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.winner_id = 0;
	}
	
	@Override
	public void run() {
		try {
			while (!this.system_status.getStart());
			this.out.println(Protocol.startGame(this.system_status.getPlayerCount(), this.player.id));
			
			while (!this.system_status.getTimeout()) {
				if (this.isInterrupted() || socket == null
						|| socket.isClosed() || !this.player.isConnected) {
					break;
				}
				
				String playerMsg = this.in.readLine();
				JSONObject jObj = Protocol.parse(playerMsg);
				
				if (jObj == null) {
					continue;
				}
				
				System.out.println(jObj.toJSONString());
				String header = Protocol.checkType(jObj);
				
				switch (header) {
					case "Request":
						bridge_id = Integer.valueOf(jObj.get("bridge").toString());
						requestBridge(bridge_id);
						break;
					case "Discard":
						bridge_id = Integer.valueOf(jObj.get("bridge").toString());
						discardRequest(bridge_id);
						break;
					case "Release":
						bridge_id = Integer.valueOf(jObj.get("bridge").toString());;
						releaseBridge(bridge_id);
						break;
					case "Acquire":
						target_id = Integer.valueOf(jObj.get("target").toString());
						acquireTarget(target_id);
						break;
					case "Leave":
						break;
				}
			}
		} catch (IOException e) {
			this.system_status.playerLost(this.player);
		}
	}
	
	public void requestBridge (int bridge_id) {
		try {
			boolean is_accepted = this.system_status.bridges[bridge_id].enter(this.player);
			if (is_accepted) {
				this.player.setOccupying(this.system_status.bridges[bridge_id]);
				this.out.println(Protocol.acceptRequest(bridge_id, this.player.id));
			} else {
				this.player.setWaiting(this.system_status.bridges[bridge_id]);
				this.out.println(Protocol.waitingRequest(bridge_id, this.player.id));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void discardRequest (int bridge_id) {
		this.system_status.bridges[bridge_id].discardWaiting(this.player);
		this.player.setWaiting(null);
		this.out.println(Protocol.discardReceived(bridge_id, this.player.id));
	}
	
	public void releaseBridge (int bridge_id) {
		try {
			this.system_status.bridges[bridge_id].leave(this.player);
			this.player.setOccupying(null);
			this.out.println(Protocol.releaseReceived(bridge_id, this.player.id));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void acquireTarget (int target_id) {
		try {
			boolean is_acquired = this.system_status.targets[target_id].acquire(this.player.id);
			if (is_acquired) {
				this.player.setScore(this.player.getScore() + 1);
				this.system_status.reduceTarget(target_id);
				this.out.println(Protocol.acquiredTarget(true, this.player.id));
				System.out.println("Player" + this.player.id + " score: " + this.player.getScore());
			} else {
				this.out.println(Protocol.acquiredTarget(false, this.player.id));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void finishGame() {
		this.winner_id = this.system_status.getWinner();
		int winner_score = this.system_status.searchPlayer(this.winner_id).getScore();
		this.system_status.printScore(this.player);
		if (winner_score == this.player.getScore()) {
			this.winner_id = this.player.id;
		}
		this.out.println(Protocol.endGame(winner_id, this.player.id));
		this.player.isConnected = false;

		try {
			socket.close();
			Thread.currentThread().interrupt();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
