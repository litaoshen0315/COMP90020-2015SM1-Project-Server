package centralserver;

public class Params {
	// The number of bridges
	public final static int NUM_BRIDGE = 12;
	
	// Server port
	public final static int PORT = 8000;
	
	// Position server port
	public final static int POS_PORT = 3000;
	
	// Waiting time before start game
	public final static int WAITING_TIME = 10000;
	
	// The entrance positions array: from player 1 to 4, from x to y
	public final static float[] ENTRANCES = {1, 0, 46, 0, 22, 47, 25, 47};
	
}
