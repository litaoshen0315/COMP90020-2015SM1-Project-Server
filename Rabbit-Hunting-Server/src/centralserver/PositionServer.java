package centralserver;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class PositionServer extends Thread {
	
	public SystemStatus system_status;
	
	ServerSocket serversocket = null;
	Socket socket = null;
	BufferedReader in;
	PrintWriter out;
	
	int player_count;
	int player_id;
	
	public PositionServer(SystemStatus system_status) {
		try {
			this.system_status = system_status;
			serversocket = new ServerSocket(Params.POS_PORT);
			this.player_count = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while(true) {
				socket = serversocket.accept();
				
				this.in = new BufferedReader(new InputStreamReader(
						this.socket.getInputStream()));
				this.out = new PrintWriter(this.socket.getOutputStream(), true);
				
				String startMsg = in.readLine();
				JSONObject jObj = Protocol.parse(startMsg);
				
				if (Protocol.checkType(jObj).equals("ConnectPos")) {
					Player player = this.system_status.searchPlayer(Integer.valueOf(jObj.get("player").toString()));
					player.setX_pos(Float.parseFloat(jObj.get("init_x").toString()));
					player.setY_pos(Float.parseFloat(jObj.get("init_y").toString()));
					
					Thread pos_con = new PosConnection(this.system_status, socket, player);
					pos_con.start();
					player_count++;
					if (player_count == this.system_status.getPlayerCount()) {
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				serversocket.close();
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
