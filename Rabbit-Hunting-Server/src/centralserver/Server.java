package centralserver;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

public class Server implements Runnable {

	public SystemStatus system_status;
	
	ServerSocket serversocket = null;
	Socket socket = null;
	BufferedReader in;
	PrintWriter out;
	
	int countPlayers;
	ArrayList<Connection> connections;
	
	// Initialize server socket.
	public Server() {
		try {
			this.system_status = new SystemStatus(Params.NUM_BRIDGE);
			serversocket = new ServerSocket(Params.PORT);
			countPlayers = 0;
			connections = new ArrayList<>();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		try {
			while(true) {
				try {
					// Accept client socket.
					socket = serversocket.accept();
					
					this.in = new BufferedReader(new InputStreamReader(
							this.socket.getInputStream()));
					this.out = new PrintWriter(this.socket.getOutputStream(), true);
					
					String startMsg = in.readLine();
					JSONObject jObj = Protocol.parse(startMsg);
					
					if (Protocol.checkType(jObj).equals("Ready")) {
						if (countPlayers < 4) {
							countPlayers++;
							Player player = new Player(countPlayers);
							this.system_status.addPlayer(player);
							Connection connection = new Connection(this.system_status, socket, player);
							connections.add(connection);
							connection.start();
							if (countPlayers >= 2) {
								serversocket.setSoTimeout(10000);
							}
							if (countPlayers == 4) {
								throw new SocketTimeoutException();
							}
						}
					}
				} catch (SocketTimeoutException e) {
					this.startGame();
					while (!this.system_status.getTimeout()) {
						if (this.system_status.checkConnection()) {
							this.system_status.setTimeout(true);
						}
					}
					if (!this.system_status.checkConnection()) {
						for (Connection c : connections) {
							c.finishGame();
						}
					}

					this.resetServer();
				}		
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				serversocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}
	
	public void startGame() {
		this.system_status.setPlayerCount(countPlayers);
		Thread pos_server = new PositionServer(this.system_status);
		pos_server.start();
		this.system_status.setStart(true);
		try {
			serversocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void resetServer() {
		try {
			this.system_status = new SystemStatus(Params.NUM_BRIDGE);
			serversocket = new ServerSocket(Params.PORT);
			countPlayers = 0;
			connections = new ArrayList<>();
			System.out.println("---------- Server Reset --------");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
