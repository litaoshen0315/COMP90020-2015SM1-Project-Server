package centralserver;

import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SystemStatus {
	
	public int num_bridges;	// The number of bridges.
	public int num_targets;	// The number of targets.
	public int winner_id;	// The winner id.
	
	ArrayList<Player> players;	// The players in the game.
	Bridge[] bridges;	// The bridge resources in the game.
	Target[] targets;	// The target resources in the game.
	
	private int player_count;	// The number of players.
	// The array indicates the status of targets: not acquired: true; acquired: false.
	private boolean[] target_status;
	private int target_left;	// The number of targets which have not been acquired.
	// True: the game has been started; False: the game has not been started.
	private boolean is_start;
	// True: the game is over; False: the game is in progress.
	private boolean is_timeout;
	
	private ReadWriteLock target_count_lock;
	private ReadWriteLock start_lock;
	private ReadWriteLock timeout_lock;

	public SystemStatus(int num_bridges) {
		this.num_bridges = num_bridges;
		this.winner_id = 0;
		
		this.players = new ArrayList<>();
		
		bridges = new Bridge[num_bridges];
		for (int i = 0; i < num_bridges; i++) {
			bridges[i] = new Bridge(i);
		}
		
		this.player_count = 0;
		this.is_timeout = false;
		
		this.target_count_lock = new ReentrantReadWriteLock();
		this.start_lock = new ReentrantReadWriteLock();
		this.timeout_lock = new ReentrantReadWriteLock();
	}
	
	public synchronized void addPlayer(Player player) {
		this.players.add(player);
	}
	
	public Player searchPlayer(int player_id) {
		for (Player p: this.players) {
			if (p.id == player_id) {
				return p;
			}
		}
		return null;
	}
	
	public void setPlayerCount(int count) {
		this.player_count = count;
		
		this.num_targets = count * 5 + 1;
		this.target_left = this.num_targets;
		targets = new Target[this.num_targets];
		for (int i = 0; i < this.num_targets; i++) {
			targets[i] = new Target(i);
		}
		
		this.target_status = new boolean[this.num_targets];
		for (int i = 0; i < this.num_targets; i++) {
			this.target_status[i] = true;
		}
	}
	
	public int getPlayerCount() {
		return this.player_count;
	}
	
	// Reduce the number of left targets. If there is no target left, game over.
	public void reduceTarget(int target_id) {
		target_count_lock.writeLock().lock();
		try {
			this.target_left--;
			this.target_status[target_id] = false;
			if (this.target_left == 0) {
				this.setTimeout(true);
			}
		} finally {
			target_count_lock.writeLock().unlock();
		}
	}
	
	public int getTargetLeft() {
		target_count_lock.readLock().lock();
		try {
			return this.target_left;
		} finally {
			target_count_lock.readLock().unlock();
		}
	}
	
	public synchronized boolean[] getTargetStatus() {
		return this.target_status;
	}
	
	// When player lost connection, set the score of the player to -1.
	// Release all the resources acquiring by this player.
	// Remove the player from any waiting list of bridges.
	public synchronized void playerLost(Player player) {
		player.isConnected = false;
		player.setScore(-1);
		player.setX_pos(-1);
		player.setY_pos(-1);
		if (player.getOccupying() != null) {
			try {
				player.getOccupying().leave(player);
				player.setOccupying(null);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (player.getWaiting() != null) {
			player.getWaiting().discardWaiting(player);
			player.setWaiting(null);
		}
	}
	
	// Check current connection status:
	// True: All players have lost the connection;
	// False: There is at least one player still in game.
	public boolean checkConnection() {
		for (Player p: this.players) {
			if (p.isConnected) {
				return false;
			}
		}
		return true;
	}
	
	public void printScore(Player player) {
		System.out.println("Player ID: " + player.id + " Score: " + player.getScore());
	}
	
	public void setStart(boolean start) {
		start_lock.writeLock().lock();
		try {
			this.is_start = start;
		} finally{
			start_lock.writeLock().unlock();
		}
	}
	
	public boolean getStart() {
		start_lock.readLock().lock();
		try {
			return this.is_start;
		} finally { 
			start_lock.readLock().unlock();
		}
	}
	
	public void setTimeout(boolean timeout) {
		timeout_lock.writeLock().lock();
		try {
			this.is_timeout = timeout;
		} finally {
			timeout_lock.writeLock().unlock();
		}
	}
	
	public boolean getTimeout() {
		timeout_lock.readLock().lock();
		try {
			return this.is_timeout;
		} finally {
			timeout_lock.readLock().unlock();
		}
	}
	
	public int getWinner() {
		int max = -1;
		int winner_id = 0;
		for (Player p: this.players) {
		  if (p.getScore() > max) {
		      max = p.getScore();
		      winner_id = p.id;
		   }
		}
		return winner_id;
	}
}
